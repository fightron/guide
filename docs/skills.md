# Fightron - Skills

* [Return to Index](../README.md)

The Skills System is used by NPCs and PCs alike.

This section is mostly important to Custom Characters, who need to acquire and improve Skills to become better fighters.

## Acquiring Skills

New characters begin with a predefined set of Skills that belong to the __Fighting Style__ they selected on character creation.

To acquire new Skills, characters can perform missions or purchase "Training Vouchers" from the martial art schools they belong to (according to their Fighting Style).

Characters that pick the "Free Form" style begin with a more basic set of Skills that belong to many different schools, but they don't really belong to any school. Because of that, they won't get discounts for Training Vouchers, and their missions will be different or more difficult.

## Improving Skills

Each Skill can be improved in different ways:

* Skill Level: each Skill can be leveled up to Level 10, using Skill Points given after a character gains a level.
* Skill Quality: each Skill can be upgraded to perform better, from Basic -> Improved -> Superior -> Perfect.
* Skill Enhancements: for every Skill Level invested, an Enhancement Slot opens up, allowing the player to slot Enhancements that improve that Skill.
* Skill Flags: change the behavior of certain skills.
* Skill Inputs: skill performance can increase or decrease depending on assigned input sequence.
* Skill Skins: change the appearance of Skills.

## Skill Levels

To use a Skill, the player needs to give it a Level (1 and above). Skills that are Level 0 (no Skill Points invested) cannot be used, with the exception of Core Skills, that are available to all characters after creation.

Characters earn Skill Points when they level up. Those points can then be used to add levels to Skills.

There is a maximum of 100 Skill Points, the same as the character max level.

## Skill Quality

Skills begin at "Basic" Quality, and they have a meter associated to them.

The meter needs to be filled by doing "Daily Practice Missions", that mostly involve performing the relevant skill a number of times in any game mode (Training and Local Versus included). The higher the quality, the longer the missions will take.

The amount of meter affects the overall performance of the skill, starting at 80% skill performance without any meter, and reaching 100% skill performance at full meter.

After a number of Daily Practice Missions have been done, the Skill can then be upgraded to the next Quality, by doing an specific mission, or purchasing and using a "Training Voucher" from the relevant school.

Daily Practice Missions needed per Quality:

* Basic: 1 day
* Improved: 5 days
* Superior: 15 days
* Perfect: 30 days

## Skill Enhancements

Skills gain "slots" with each level invested on them, for a maximum of 10 slots.

Enhancements can then be placed into those slots, further improving that particular Skill.

Enhancements can be crafted or bought from in-game stores using in-game credits, or earned in Events or Tournaments.

## Skill Flags

Certain Skills have "flags" that change their behavior in combat.

This applies mostly to Attack Skills. Players can pick a number of flags that change a skill in certain ways.

Multiple flags can be chosen, and they often have cumulative penalties associated with them to counter-balance the given benefits.

A few examples (tentative values):

|Flag|Effect|Penalty|
|---|---|---|
|Cancellable into Specials|Makes Skill cancellable into special moves|10%|
|Cancellable into Normals|Makes Skill cancellable into normals|20%|
|Hit-Cancellable into Jump|Makes Skill cancellable into Jump upon hit|20%|
|Block-Cancellable into Jump|Makes Skill cancellable into Jump upon block|30%|
|Cancellable into Dash/Run|Makes Skill cancellable into Dash or Run|30%|
|Block-Reversable|Skill can be used to get out of blockstun (requires "Block Reversal" Mechanic)|50%|

If total penalty amount goes above 100%, the skill becomes unusable until some flags are removed.

## Skill Inputs `←↙↓↘→↗↑↖`

Skills can be further modified by the input sequence assigned to them.

Let's say the character earns a new Skill called Fireball. After giving it some Levels, the player must assign an input sequence to the new Skill.

A common input sequence for a projectile would be: `↓↘→ L`

But, if the player feels like, he can assign the following (harder) sequence: `←↙↓↘→ L`. If he does so, he will get a small bonus to skill performance.

On the other hand, he can assign an easier input sequence such as: `→ S`. Because it's easier, the skill performance would get a penalty, relative to how easy the new sequence it compared to a reasonable default.

Some input sequences give bigger bonuses, like the infamous "Pretzel Motion": `↙→↘↓↙←↘`. (The brave shall be rewarded.) But also note that difficult input sequences also affect [__A.I. builds__](./ai.md), as robots with low "Execution" skill can miss those complex inputs, or take more time to perform them, which may affect their performance.

Input sequences can also be flagged as "pristine". Pristine inputs do not accept intermediate inputs that do not belong to the sequence. A known approach to perform airborne skills would be to use the "Tiger Knee Motion". Let's say some Skill using `↓↙← M` can be performed in the air. Using the "Tiger Knee Motion" approach, one would enter `↓↙←↖ M` to make the character jump and the Skill would still execute because all inputs from the sequence were entered. However, flagging input sequences as "pristine" would make the dirty input (`↖`) break the sequence and prevent it from completing. The player would then be required to jump first, then cleanly enter the complete sequence. Assigning pristine input sequences to Skills gives them a small performance boost.

## Core Skills

Core Skills are available to all characters upon creation, and are available to use from Level 0, unlike normal Skills.

Core Skills usually unlock [__Mechanics__](./mechanics.md).

### Core Skill: Stance

The __Stance__ Core Skill improves the character's overall performance.

__Daily Practice__: play any match for 5/15/30/60 seconds.

__Quality Upgrade__: slightly reduces the size of hurtboxes from standing and crouching.

|Level|Unlock|
|---|---|
|1|"Shake Off" Mechanic|
|2|"Quick Rise" Mechanic|
|3|"Chip Damage I" Mechanic|
|4|"Chip Damage II" Mechanic|
|5|???|
|6|"Roll Recovery" Mechanic|
|7|"Chip Damage III" Mechanic|
|8|???|
|9|???|
|10|"Chip Damage IV" Mechanic|

### Core Skill: Block

The __Block__ Core Skill grants additional shielding mechanics to a character.

__Daily Practice__: block 3/6/12/30 times.

__Quality Upgrade__: reduces blockstun frames. The amount varies according to the blockstun strength (e.g., light blockstuns get lesser reductions).

|Level|Unlock|
|---|---|
|1|"Chip Damage Protection" Mechanic|
|2|"Chip Damage Conversion" Mechanic|
|3|"Shield Surge" Mechanic|
|4|???|
|5|"Repulsion Shield" Mechanic|
|6|"Shield Reflect" Mechanic|
|7|"Shield Regeneration I" Mechanic|
|8|"Shield Regeneration II" Mechanic|
|9|"Block Reversal" Mechanic|
|10|"[__Omni-Shield__](#unique-skill-omni-shield)" Unique Skill|

### Core Skill: Resilience

The __Resilience__ Core Skill grants mechanics that make a character more sturdy.

__Daily Practice__: get hit by 1/3/6/12 skills.

__Quality Upgrade__: reduces hitstun frames. The amount varies according to the hitstun strength (e.g., light hitstuns get lesser reductions).

|Level|Unlock|
|---|---|
|1|???|
|2|???|
|3|???|
|4|???|
|5|"Breaker" Mechanic|
|6|"Health Regeneration I" Mechanic|
|7|"Armor" Mechanic|
|8|"Health Regeneration II" Mechanic|
|9|"Health Regeneration III" Mechanic|
|10|???|

### Core Skill: Concentration

The __Concentration__ Core Skill grants mechanics that make a character deal better with stun state.

__Daily Practice__: recover 10/20/50/100% of your Focus Points.

__Quality Upgrade__: reduces stun state recovery frames (post-mashing).

|Level|Unlock|
|---|---|
|1|???|
|2|???|
|3|???|
|4|???|
|5|???|
|6|???|
|7|???|
|8|"Focus Regeneration I" Mechanic|
|9|"Focus Regeneration II" Mechanic|
|10|???|

### Core Skill: Stamina

The __Stamina__ Core Skill grants mechanics that make a character better generate Special Points.

Without any points in this Core Skill, the character can only generate Special Points by successfully hitting the enemy with Special Skills.

__Daily Practice__: generate 10/20/50/100 Special Points.

__Quality Upgrade__: increase maximum Special Points (relative to the __Intelligence__ Attribute):
* Improved: 30% of Intelligence
* Superior 60% of Intelligence
* Perfect: 100% of Intelligence
* Example: a character with 40 Intelligence will get +24 Special Points (40 * 0.6) when this skill is at Superior Quality.

|Level|Unlock|
|---|---|
|1|"Special Generation I" Mechanic|
|2|"Special Generation II" Mechanic (generates SP when enemy blocks Special Skills)|
|3|"Special Generation III" Mechanic (generates SP when character is hit)|
|4|"Special Generation IV" Mechanic (generates SP when Special Moves whiff)|
|5|"Special Charge" Mechanic|
|6|"Special Generation V" Mechanic (generates SP when character blocks)|
|7|"Special Generation VI" Mechanic (generates SP when enemy blocks Normals)|
|8|"Special Drain Protection" Mechanic|
|9|???|
|10|"Special Generation VII" Mechanic (generates SP when Normals whiff)|

### Core Skill: Pacing

The __Pacing__ Core Skill improves ground movement on a character.

__Daily Practice__: move on the ground (walk, dash, run) by a cumulative distance of 5/10/25/50 meters.

__Quality Upgrade__: slightly reduces the size of hurtboxes from ground movement skills.

|Level|Unlock|
|---|---|
|1|"Dash I" Mechanic|
|2|"Backdash I" Mechanic|
|3|"Running" Mechanic|
|4|???|
|5|"Roll Evasion" Mechanic|
|6|???|
|7|"Backdash II" Mechanic (cancellable into normals)|
|8|"Dash Cancel I" Mechanic (cancellable into normals)|
|9|"Dash Cancel II" Mechanic (cancellable into crouching - can't dash right afterwards though)|
|10|"Wave Dash" Mechanic|

### Core Skill: Air Superiority

The __Air Superiority__ Core Skill gives more aerial options to a character.

__Daily Practice__: jump 3/6/12/30 times.

__Quality Upgrade__: reduces recovery frames from landing (starting at 3 frames). Basic: 0, Improved -1, Superior -2, Perfect -3.

|Level|Unlock|
|---|---|
|1|"Air Combo" Mechanic|
|2|"Air Recovery" Mechanic|
|3|"Air Blocking" Mechanic|
|4|"Wall Jump" Skill|
|5|"Long Jump" Skill|
|6|"Air Dash" Skills|
|7|"Double Jump" Mechanic|
|8|???|
|9|"Short Jump" Skill|
|10|Reduces required height for aerial Skills to execute|

### Core Skill: Grappling

The __Grappling__ Core Skill enhances throws on a character.

__Daily Practice__: throw 1/3/6/12 times.

__Quality Upgrade__: increases the frame window of Throw Escapes.

|Level|Unlock|
|---|---|
|1|"Throw Escape" Mechanic|
|2|"Throw Clash" Mechanic|
|3|???|
|4|???|
|5|"Grapple" Mechanic|
|6|???|
|7|???|
|8|???|
|9|???|
|10|"Throw Reversal" Mechanic|

### Core Skill: Taunt

The __Taunt__ Core Skill gives taunting special effects.

__Daily Practice__: taunt 1/3/6/12 times.

__Quality Upgrade__: decreases the amount of frames until Taunt becomes cancellable.

|Level|Unlock|
|---|---|
|1|"Taunt Recovery" Mechanic|
|2|???|
|3|???|
|4|???|
|5|"Taunt Poking" Mechanic|
|6|???|
|7|???|
|8|???|
|9|"Aerial Taunt" Skills|
|10|"Taunt Drain" Mechanic|

## Active Skills

Active Skills need to be acquired by the character as they level up, either by purchasing Training Vouchers at schools, or unlocking them in missions.

Active Skills include all attacks (normals, specials, supers) as well as movement (dashes, running, etc) and utility (chargers, taunts, breakers, etc) skills.

As with Core Skills, each skill level invested in an Active Skill opens up an Enhancement Slot, which can be used to further improve that particular skill.

The bonuses below are default for all Active Skills, for a total Performance bonus of 100% at Level 10. (Tentative values.)

|Level|Bonus|
|---|---|
|1|Skill Performance +10%|
|2|Skill Performance +10%|
|3|Skill Performance +10%|
|4|Skill Performance +10%|
|5|Skill Performance +10%|
|6|Skill Performance +10%|
|7|Skill Performance +10%|
|8|Skill Performance +10%|
|9|Skill Performance +10%|
|10|Skill Performance +10%|

## Unique Skills

Certain Core Skills or Enhancements unlock Unique Skills. Those Skills have no Levels, and they're available as long as the requirement is valid.

### Unique Skill: Omni-Shield

__Requirement__: Block Lv 10

__Daily Practice__: activate Omni-Shield 1/5/15/30 times.

__Quality Upgrade__: reduces Shield Points upkeep and blocking costs.

The __Omni-Shield__ Unique Skill creates a shield that completely encompasses the character upon holding down __L+M__ buttons. It can be activated during normal or crouching stance, and during certain status, such as downed.

While active, the Omni-Shield continuously drains Shield Points at a fast rate and automatically blocks all attacks from all sides, including highs and lows and cross-ups.

Blocking during this state costs a large amount of Shield Points. This cost can be reduced slightly by improving the Skill Quality.

If there are not enough Shield Points to keep it up or the required amount to block a single attack, the Skill deactivates.

The character is not able to execute any Active Skills while the Omni-Shield is up. They can only move around to escape danger, including dashes, running, and jumping.

## Skill Skins

Players can give different visuals to Skills through giving it a "Skin".

Skins are often tied to the __Fighting Style__ chosen upon character creation. For __Free Form__ characters, there is a much larger variety of skins to choose from.

Setting a Skill Skin does not affect its behavior other than how it looks. In other words, skill performance, hit/hurtboxes, framedata, and such, won't be altered.

Example: a __Roundhouse Kick__ is available to multiple Fighting Styles. Each style has a default Skin that makes the Roundhouse Kick looks more like it belongs to the Fighting Style. Savateurs, for example, have a different upper body position than karatekas, while their default performance is exactly the same. 

Skins can also update the skill name, so a Roundhouse Kick for Savateurs will be called "Fouette", for Karatekas, "Mawashi Geri", and so on. When there are multiple variants within the same fighting style, the name will be further updated, such as "Fouette A", "Fouette B", etc.

Skill Skins can be purchased from some in-game stores and from the gold store, or awarded during quests or events.

____

[__Next: Mechanics__](./mechanics.md)

[__Return to Index__](../README.md)
