# Fightron - Game Guide

_Fightron_ is a fighting game currently in development.

## Table of Contents

1. [__Introduction__](./docs/introduction.md)
    * What is Fightron?
    * FAQ
2. __Game Systems__
    * [__Controls__](./docs/controls.md)
    * [__Meters__](./docs/meters.md)
    * [__Attributes__](./docs/attributes.md)
    * [__Skills__](./docs/skills.md)
    * [__Mechanics__](./docs/mechanics.md)
    * [__Enhancements__](./docs/enhancements.md)
3. [__Game Modes__](./docs/modes.md)
    * Story
    * Arcade
    * Versus
    * Training
    * Multiplayer
4. [__Characters__](./docs/characters.md) and [__Factions__](./docs/factions.md)
    * Artists: 
        * [__Tera__](./docs/characters/tera.md)
        * Andromeda
        * Carrion
        * Tick
        * Triton (Faction Boss)
    * Enforcers: 
        * [__Proto__](./docs/characters/proto.md)
        * Cybel
        * Hammer
        * Talon
        * Titania (Faction Boss)
    * Rogues: 
        * [__Jet__](./docs/characters/jet.md)
        * Sparkle
        * Blackout
        * Amp
        * Futura (Faction Boss)
   * No Faction:
        * Void (Final Boss)
5. __Creation Modes__
    * [__Create a Character__](./docs/customization.md)
    * [__Create an A.I.__](./docs/ai.md)
6. __Lore__
    * The City of Nettronia
    * Nettrons, Fightrons, and Humans
    * The Guardians
7. [__Development__](./docs/development.md)
    * The Team
    * Motivation
    * Platform
    * Components
    * [__Roadmap__](./docs/roadmap.md)
    * [__Balance__](./docs/balance.md)
    * [__Monetization__](./docs/monetization.md)

## Disclaimer

All information is subject to change.
